export const API = 'https://pokeapi.co/api/v2/';
export const CUSTOM_POKEMON = `${API}pokemon/{id or name}/`;
export const ALL_POKEMON = `${API}`;
